package com.cain.threadpool.service;

import java.util.concurrent.Future;

import com.cain.threadpool.entity.CainResult;
import com.cain.threadpool.entity.FaceVerificationResult;
import com.cain.threadpool.entity.HelloEntity;
import com.cain.threadpool.entity.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

/**
 * @author Cain.chi
 * @date 2019/7/2 17:43
 * @description
 */
@Service
public class HelloService {

    Logger logger = LoggerFactory.getLogger(HelloService.class);

    /**
     * @Async标注的方法，称之为异步方法；这些方法将在执行的时候，
     * 将会在独立的线程中被执行，调用者无需等待它的完成，即可继续其他的操作。
     */
//    @Async("myThreadPool") // 参数为线程池配置时的方法名即对应的bean的id
//    @Async("myThreadPoolTaskExecutor") // 参数为线程池配置时的方法名即对应的bean的id
    @Async
    public void sayHello() {
        logger.info("start say hello");
        System.out.println(Thread.currentThread().getName());
        System.out.println("hello");
        logger.info("end say hello");
    }

    @Async
//    @Async("myThreadPoolTaskExecutor")
    public Future<HelloEntity> getHelloString() {
        logger.info("start getHelloString");
        HelloEntity helloEntity = new HelloEntity();
        helloEntity.setHelloStr("Say hello to little wang");
        System.out.println(Thread.currentThread().getName());
        logger.info("end getHelloString");
        // 测试异常是否会传递给caller
        //        if(true){
        //            throw new NullPointerException("111");
        //        }
        return new AsyncResult<>(helloEntity);
    }

}
