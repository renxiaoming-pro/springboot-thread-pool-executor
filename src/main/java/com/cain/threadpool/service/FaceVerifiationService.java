package com.cain.threadpool.service;

import com.cain.threadpool.entity.FaceVerificationResult;
import com.cain.threadpool.entity.UserInfo;
import org.springframework.stereotype.Service;

/**
 * @author Cain.chi
 * @date 2019/7/4 17:40
 * @description 人脸识别service
 */
@Service
public class FaceVerifiationService {

    public FaceVerificationResult getResult(UserInfo userInfo) {
        FaceVerificationResult faceVerificationResult = new FaceVerificationResult();
        // 成功返回 100
        faceVerificationResult.setCode(100);
        faceVerificationResult.setMessage("success");
        try {
            // 模拟调用人脸识别接口的http请求消耗的时间
            // 如果需要模拟异常的情况那么时间应该在 4(主方法的逻辑处理时间) + 3(允许的超时时间) 也就是7s以上才可以
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return faceVerificationResult;
    }
}
