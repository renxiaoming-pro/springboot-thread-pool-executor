package com.cain.threadpool.entity;

/**
 * @author Cain.chi
 * @date 2019/7/4 17:41
 * @description 用户信息DTO
 */
public class UserInfo {

    private Integer id;

    private String name;

    private String photo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
