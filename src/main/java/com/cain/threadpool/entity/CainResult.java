package com.cain.threadpool.entity;

/**
 * @author Cain.chi
 * @date 2019/7/4 17:45
 * @description 共同返回结果Result
 */
public class CainResult {

    private Integer code;

    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
