package com.cain.threadpool.entity;

/**
 * @author Cain.chi
 * @date 2019/7/3 10:44
 * @description helloDTO
 */
public class HelloEntity {
    private String helloStr;

    public String getHelloStr() {
        return helloStr;
    }

    public void setHelloStr(String helloStr) {
        this.helloStr = helloStr;
    }
}
