package com.cain.threadpool.entity;

/**
 * @author Cain.chi
 * @date 2019/7/4 17:38
 * @description 人脸识别结果Result
 */
public class FaceVerificationResult {

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
