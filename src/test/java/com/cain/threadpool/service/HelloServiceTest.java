package com.cain.threadpool.service;

import java.util.concurrent.Future;

import com.cain.threadpool.ThreadPoolApplication;
import com.cain.threadpool.entity.HelloEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * HelloService Tester.
 *
 * @author Cain.Chi
 * @version 1.0
 * @since <pre>七月 2, 2019</pre>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ThreadPoolApplication.class)
public class HelloServiceTest {

    @Autowired
    HelloService helloService;

    /**
     * Method: sayHello()
     */
    @Test
    public void testSayHello() throws Exception {
        helloService.sayHello();
    }

    /**
     * Method: getHelloString()
     */
    @Test
    public void testGetHelloString() throws Exception {
        Future<HelloEntity> helloString = helloService.getHelloString();
        HelloEntity helloEntity = helloString.get();
        System.out.println(helloEntity.getHelloStr());
    }

} 
