package com.cain.threadpool.service;

import java.time.Instant;

import com.cain.threadpool.ThreadPoolApplication;
import com.cain.threadpool.entity.UserInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Cain.Chi
 * @date 2019/7/4 17:59
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ThreadPoolApplication.class)
public class LoginServiceTest {

    @Autowired
    LoginService loginService;

    @Test
    public void login() {
        UserInfo userInfo = new UserInfo();
        long start = Instant.now().toEpochMilli();
        loginService.login(userInfo);
        long end = Instant.now().toEpochMilli();
        System.out.println(end - start);
    }
}